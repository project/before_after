Overview
Before/After Field Formatter will provide a field formatter for image fields so that the images uploaded for an image field would be rendered as a Before and After Images.


Installation and Configuration
1. Download and Enable the module similar to other Drupal modules
2. Visit any Image fields in Display settings, you will be able to find the Before/After Field Formatter, Select this one and you would be able to select image styles and another config.
Ex: admin/structure/types/manage/article/display


Available options

    Image style
    Before After Buttons
   
Integration
This module works well with.
Modules like Views and other Core modules, where image fields are used. (We can select Before/After Field Formatter in view image field formate also)
   
Note: This Module will work if image field has 2 images, one which shows before condition and second which show after condition.
(More than 2 images or less than 2 will disable the Before-After functionality.)


MAINTAINERS
-----------

Current maintainers:
 * Swati Chouhan (Drupal Developer) - https://www.drupal.org/u/swatichouhan012